<?php
namespace App\Http\Controllers;
use App\Models\Film;
use Illuminate\Http\Request;

class FilmController extends Controller {
    // list all film
    function list(Request $req) {
        $title = $req->title;
        $description = $req->description;
        //echo $title;
        $query = Film::whereNotNull('film_id');

        if (! empty($title)){
            $query = $query->where('title', 'like', "%$title%");
        }

        if(! empty($description)) {
            $query = $query->where('description', 'like', "%$description%");
        }

        $films = $query->paginate(20);
        return view('film.list', ['films' => $films]);
    }
}
