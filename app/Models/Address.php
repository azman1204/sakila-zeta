<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Address extends Model {
    public $table = 'address';
    public $primaryKey = 'address_id';

    function city() {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'city_id');
    }
}
