<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Store extends Model {
    public $table = 'store';
    public $primaryKey = 'store_id';

    function address() {
        return $this->belongsTo(\App\Models\Address::class, 'address_id', 'address_id');
    }
}
