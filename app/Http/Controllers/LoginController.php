<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    // show login screen
    function login() {
        //echo \Hash::make('1234'); // encrypt password
        return view('login.form');
    }

    // authenticate user after submitting from login screen
    function auth(Request $req) {
        $username = $req->username;
        $password = $req->password;
        $credentials = ['username' => $username, 'password' => $password];
        if (Auth::attempt($credentials)) {
            // login berjaya
            session(['username' => $username]); // set data ke dlm session
            return redirect()->route('home');
        } else {
            // login gagal. with() = flash session
            return redirect()->back()->with('err', 'Login Gagal');
        }
    }

    function logout() {
        Auth::logout();
        session()->flush(); // remove all session
        return redirect('/login');
    }
}
