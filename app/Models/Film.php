<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model {
    public $table = 'film';
    public $primaryKey = 'film_id';

    function language() {
        return $this->belongsTo(\App\Models\Language::class, 'language_id', 'language_id');
    }

    function actor() {
        return $this->belongsToMany(\App\Models\Actor::class, 'film_actor', 'film_id', 'actor_id');
    }
}
