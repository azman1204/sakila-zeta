<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Language extends Model {
    public $table = 'language';
    public $timestamps = false;
    // data yg boleh mass insert
    public $fillable = ['name'];
}
