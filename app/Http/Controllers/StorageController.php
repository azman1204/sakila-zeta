<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller {
    // create file
    function createFile() {
        Storage::disk('local')->put('1/sample.txt', 'Hello World...');
        Storage::disk('public')->put('2/sample2.txt', 'Hello World2...');
        echo "File has been created...";
    }

    function getFile() {
        $content = Storage::get('laravel.png');
        // download image
        return Storage::download('laravel.png');
        //return response($content)->header('Content-type', 'image/png');
        //echo $content;
    }
}
