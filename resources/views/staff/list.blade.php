@extends('layouts.app')
@section('content')

<a href="/staff/create" class="btn btn-sm btn-primary">Create New Staff</a>

<table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th>First name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>City</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($staffs as $staff)
        <tr>
            <td>{{ $staff->first_name }}</td> <!-- komen html -->
            <td>{{ $staff->last_name }}</td>
            <td>{{ $staff->store->address->address }}</td>
            <td>{{ $staff->store->address->city->city }}</td>
            <td>
                <a class="btn btn-sm btn-success" href="/staff/edit/{{ $staff->staff_id }}">Edit</a>
                <a class="btn btn-sm btn-danger" onclick="return confirm('Anda pasti ?')" href="/staff/delete/{{ $staff->staff_id }}">Delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection
