<?php
namespace App\Http\Controllers;
use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Store;

class StaffController extends Controller {
    // list all staff
    function list() {
        $staffs = Staff::all();
        return view('staff.list', ['staffs' => $staffs]);
    }

    // show form to insert a new staff
    function create() {
        $staff = new Staff();
        $stores = Store::all();
        return view('staff.form', compact('staff', 'stores'));
    }

    // edit staff info
    function edit($staff_id) {
        $staff = Staff::find($staff_id);
        $stores = Store::all();
        return view('staff.form', compact('staff', 'stores'));
        //return view('staff.form', ['staff' => $staff]);
    }

    // insert or update
    function store(Request $req) {
        //dd($req->all()); // all() - return all data from form / URL as array
        $staff_id = $req->staff_id;

        if(empty($staff_id)) {
            // insert
            $staff = new Staff();
            $staff->created_by = session('username');
        } else {
            // update
            $staff = Staff::find($staff_id);
        }

        $staff->first_name = $req->first_name;
        $staff->last_name = $req->last_name;
        $staff->email = $req->email;
        $staff->password = $req->password;
        $staff->address_id = 1;
        $staff->username = $req->username;
        $staff->active = $req->active;
        $staff->store_id = $req->store_id;

        // validation
        $rules = [
            'username'  => 'required|min:4',
            'first_name'=> 'required',
            'last_name' => 'required',
            'email'     => 'required|email',
            'password'  => 'required|min:4',
            'store_id'  => 'required',
            'picture'   => 'mimes:jpeg,jpg,png,gif,svg|max:100'
        ];

        $msg = [
            'username.required' => 'ID Pengguna wajib diisi',
            'username.min'      => 'ID Pengguna mestilah sekurang-kurang nya 4 karakter',
            'store_id.required' => 'Store wajib diisi',
            'picture.size'      => 'Saiz gambar mestilah kurang dari 10Kb'
        ];

        $v = Validator::make($req->all(), $rules, $msg);

        if ($v->fails()) {
            // gagal validation. show back the form with error message
            $stores = Store::all();
            return view('staff.form', compact('staff', 'stores'))->withErrors($v);
        } else {
            // success. insert / update data
            if($req->hasFile('picture')) {
                $photo = $req->file('picture')->store('photo');
                $staff->photo = $photo;
            }

            $staff->save();
            return redirect('/staff/list');
        }
    }

    function delete($staff_id) {
        Staff::find($staff_id)->delete();
        return redirect('/staff/list');
    }

    function image(Request $req) {
        $content = \Illuminate\Support\Facades\Storage::get($req->location);
        return response($content)->content('Content-type', 'image/png');
    }
}
