<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MailerController extends Controller
{
    function send() {
        // return view('film.list', data)
        \Mail::send('mail.sample', ['nama' => 'azman'], function($message) {
            $message->to('azman@gmail.com')
            ->subject('Testing Email Laravel')
            ->from('azman@yahoo.com');
        });
    }

    function sendWithAttachment() {
        // return view('film.list', data)
        \Mail::send('mail.sample', ['nama' => 'azman'], function($message) {
            $message->to('azman@gmail.com')
            ->subject('Testing Email With Attachment')
            ->from('azman@yahoo.com');
            $file_path = storage_path() . "/app/laravel.png";
            $message->attach($file_path);
        });
    }
}
