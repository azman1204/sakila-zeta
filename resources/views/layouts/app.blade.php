<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Sakila CD Store</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>

                    {{-- @if(\Auth::user()->role == 'admin')
                        <li class="nav-item">
                            <a class="nav-link" href="/staff/list">Staff</a>
                        </li>
                    @endif --}}

                    @can('create staff')
                        <li class="nav-item">
                            <a class="nav-link" href="/staff/list">Staff</a>
                        </li>
                    @endcan

                    @can('add payment')
                        <li class="nav-item">
                            <a class="nav-link" href="/staff/list">Receive Payment</a>
                        </li>
                    @endcan

                    @can('view report')
                        <li class="nav-item">
                            <a class="nav-link" href="/staff/list">Report</a>
                        </li>
                    @endcan

                    <li class="nav-item">
                        <a class="nav-link" href="/film/list">Film</a>
                    </li>

                    @if(Auth::check())
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Logout {{ Auth::user()->username }}</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>
</body>

</html>
