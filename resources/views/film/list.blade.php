@extends('layouts.app')
@section('content')

<form action="/film/list" method="post">
    @csrf
    Title : <input type="text" name="title">
    Description : <input type="text" name="description">
    <input type="submit" value="Search" class="btn btn-primary">
</form>

<table class="table table-bordered table-striped table-hover">
    <tr>
        <th>No</th>
        <th>Title</th>
        <th>Description</th> <!-- komen disini -->
        <th>Language</th>
        <th>Actor</th>
    </tr>
    @php $no = $films->firstItem() @endphp
    @foreach($films as $film)
    <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $film->title }}</td>
        <td>{{ $film->description }}</td>
        <td>{{ $film->language->name }}</td>
        <td>
            @foreach($film->actor as $actor)
                {{ $actor->first_name }} <br>
            @endforeach
        </td>
    </tr>
    @endforeach
</table>
{{ $films->links() }}
@endsection
