<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PakGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(! \Auth::check()) {
            // cubaan hacking. tiada permission
            return redirect('/login')->with('err', 'No Permission..');
        }

        return $next($request);
    }
}
