<?php
namespace App\Http\Controllers;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class PrivController extends Controller {
    function createRole() {
        $role = Role::create(['name' => 'admin']);
        $role = Role::create(['name' => 'store']);
        $role = Role::create(['name' => 'counter']);
        $role = Role::create(['name' => 'finance']);
    }

    function createPermission() {
        Permission::create(['name' => 'add payment']);
        Permission::create(['name' => 'edit payment']);
        Permission::create(['name' => 'cancel payment']);
        Permission::create(['name' => 'add staff']);
        Permission::create(['name' => 'view report']);
        Permission::create(['name' => 'add film']);
    }

    function setRolePermission() {
        // role counter
        $role = Role::where('name', 'counter')->first();
        $permission = Permission::where('name', 'add payment')->first();
        $role->givePermissionTo($permission);

        // role finance
        $role = Role::where('name', 'finance')->first();
        $permission = Permission::where('name', 'edit payment')->first();
        $role->givePermissionTo($permission);

        $permission = Permission::where('name', 'view report')->first();
        $role->givePermissionTo($permission);
    }

    function assignRoleToUser() {
        $user = User::where('username', 'Mike')->first();
        $user->assignRole('counter');

        $user = User::where('username', 'Jon')->first();
        $user->assignRole('finance');
    }

    // show semua permission bg org yg logged-in ini
    function showAllPermission() {
        $user = \Auth::user();
        $permissions = $user->getAllPermissions();
        if($user->can('edit payment')) {
            echo "yes";
        } else {
            echo "No";
        }
        dd($permissions->toArray());
    }
}
