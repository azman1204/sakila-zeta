<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\StorageController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PrivController;
use App\Http\Controllers\MailerController;

// send email
Route::get('/mail-send', [MailerController::class, 'send']);
Route::get('/mail-send-attachment', [MailerController::class, 'sendWithAttachment']);

Route::get('/create-role', [PrivController::class, 'createRole']);
Route::get('/create-permission', [PrivController::class, 'createPermission']);
Route::get('/set-role-permission', [PrivController::class, 'setRolePermission']);
Route::get('/assign-role-to-user', [PrivController::class, 'assignRoleToUser']);
Route::get('/show-all-permission', [PrivController::class, 'showAllPermission']);

// Login
Route::get('login', [LoginController::class, 'login']);
Route::post('login', [LoginController::class, 'auth']);
Route::get('logout', [LoginController::class, 'logout']);

// Storage
Route::get('/storage/create', [StorageController::class, 'createFile']);
Route::get('/storage/get', [StorageController::class, 'getFile']);

// Film. any() support GET dan POST
Route::any('/film/list', [FilmController::class, 'list'])->name('home');

// staff
Route::middleware(['pakguard'])->prefix('/staff')->group(function() {
    Route::get('/list', [StaffController::class, 'list']);
    Route::get('/create', [StaffController::class, 'create']);
    // on submit form for insert or update
    Route::post('/store', [StaffController::class, 'store']);
    Route::get('/edit/{staff_id}', [StaffController::class, 'edit']);
    Route::get('/delete/{staff_id}', [StaffController::class, 'delete']);
    Route::get('/image', [StaffController::class, 'image']);
});
